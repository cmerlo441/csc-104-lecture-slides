% !TEX root = lecture.tex

\section{Day \thesection: Tracing a Python Program}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I need to determine what a Python program does \textit{without using a computer}}
    \tools{Pen, paper, some Python code}
    \newtools{Code tracing strategies}
    How do I figure out what this program is supposed to do?
\end{frame}

\begin{frame}{Day \thesection\ Tracing a Python Program}
\begin{itemize}[<+- | alert@+>]
    \item What does it mean to trace a Python program?
    \item Tracing a program is the skill of reading a line of code and knowing what it will do without having to run the code on the computer
    \item This is a valuable skill to have when \alert{debugging} a program
    \item It is often important to be able to tell what a program \textit{should} do, especially when it isn't doing that
\end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a Python Program}
    \begin{itemize}[<+- | alert@+>]
        \item When tracing a program, a few key things are important:
        \begin{itemize}[<+- | alert@+>]
            \item We must be aware of what \alert{input} our program receives, and what happens to it
            \item We must know what each \alert{variable} contains, when it changes, and \textit{why it changes}
            \item We must know precisely what gets \alert{displayed} on the screen, and in what order
        \end{itemize}
        \item Depending on the program and its complexity, you might find it useful to create a list of events, or to fill in a table
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracking Variable Changes}
    \begin{itemize}[<+- | alert@+>]
        \item When tracking the changes to a variable in a Python program, remember that a variable can only store one value at a time
        \item When the same variable appears on the left-hand side of a new assignment statement, the old value of that variable gets destroyed, and only the new value remains
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracking Variable Changes}
    \begin{itemize}[<+- | alert@+>]
        \item Consider this Python program:\\
        \mintinline{python}{x = 5}\\
        \mintinline{python}{x = 10}
        \item On the first line of code, the variable \mintinline{python}{x} gets \textit{initialized} -- it comes into existence, and is immediately assigned the value 5
        \item On the second line of code, no new variable is created
        \item \mintinline{python}{x} still exists, but the value 10 is stored in it
        \item When this happens, the 5 is thrown away
        \item During a tracing exercise, you would be expected to make note of both events: \mintinline{python}{x} stored a 5, and then later it was made to store 10
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracking Variable Changes}
    \begin{itemize}[<+- | alert@+>]
        \item Remember that we can use a mathematical expression on the right-hand side of an assignment statement
        \item Note that if the program were changed from:\\
        \mintinline{python}{x = 5}\\
        \mintinline{python}{x = 10}\\
        To:\\
        \mintinline{python}{x = 5}\\
        \mintinline{python}{x = x + 5}\\
        The results of your trace would be exactly the same
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Using a Table}
    \begin{itemize}[<+- | alert@+>]
        \item Some programmers choose to fill in a table as the program runs to keep track of events
        \item In such a table, make sure that \alert{later} events are \alert{to the right} or \alert{below} earlier events
        \item Consider this program:\\
        \inputminted{py}{trace1.py}
        \item Notice that \texttt{x} changes, then \texttt{y}, and then \texttt{y} changes again before \texttt{x} does
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Using a Table}
    \begin{itemize}[<+- | alert@+>]
        \item A programmer might fill in a table to trace the previous program this way:\\
        \begin{center}
            \begin{tabular}{cc}
            \textbf{x} & \textbf{y}\\
            \hline
            5 & 10 \\
            & 20 \\
            35 &\\
            \hline
            \end{tabular}
        \end{center}
        \item Remember: \alert{later} events appear \alert{to the right} or \alert{below} earlier events
        \item We can read a table that has been properly filled out to recreate the order of events
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing Program Output}
    \begin{itemize}[<+- | alert@+>]
        \item Remember that only \mintinline{python}{print()} statements cause output to appear on the screen
        \item Let's add two output statements to the previous program:\\
        \inputminted{python}{trace2.py}
        \item A table that traces that program should look like this:\\
        \begin{center}
            \begin{tabular}{ccc}
            \textbf{x} & \textbf{y} & \textbf{Output}\\
            \hline
            5 & 10 & 10 \\
            & 20 \\
            35 & & 20\\
            \hline
            \end{tabular}
        \end{center}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing Program Input}
    \begin{itemize}[<+- | alert@+>]
        \item The previous programs will always perform the same tasks
        \item That won't be true when you use the \mintinline{python}{input()} function to assign values to variables
        \item You will often be asked to trace a program that takes user input multiple times, using different input values
        \item Make sure to pay attention to how different values affect the program!
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Homework}
    \begin{itemize}
        \item Finish the \alert{Name Processing Homework} before next time, and remember to submit it on time
        \item Finish the exercises in \alert{Tracing Python Program Code Activity} before next time; we will discuss your results in class
        \item Set up \texttt{graphics.py} as explained in the Lecture Notes \textit{before} the next class
    \end{itemize}
\end{frame}