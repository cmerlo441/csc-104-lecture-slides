% !TEX root = lecture.tex

\section{Day \thesection: Scripts and Modules}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I need to manage a lot of Python code}
    \tools{Pen, paper}
    \newtools{Python scripts, Python modules}
    How do I make my program run a program?
\end{frame}

\begin{frame}{Day \thesection\ Scripts}
    \begin{itemize}[<+- | alert@+>]
        \item So far, all the code we've typed into IDLE has run in interactive mode
        \item Python programs can also exist within \textit{scripts} and \textit{modules}
        \item Let's make a script
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Scripts}
    \begin{itemize}[<+- | alert@+>]
        \item Start IDLE
        \item From the File menu, choose New File
            \begin{itemize}[<+- | alert@+>]
                \item In the future, instructions like that will be written as ``Choose File | New File'' or just ``File | New File''
            \end{itemize}
        \item This will create a second IDLE window
        \item Let's type a program into this second window
        \item Start with a \alert{\textbf{comment}} containing your name
        \item Comments are notes we write in our code to other programmers, that are ignored by the interpreter when the program runs
        \item A comment in Python starts with a \texttt{\#} character, and ends at the end of the line
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Scripts}
    \begin{itemize}[<+- | alert@+>]
        \item The code for this script should look like this:\\
        \inputminted{py}{name.py}
        \item Choose File | Save to save this file
        \begin{itemize}[<+- | alert@+>]
            \item Use the filename \texttt{name.py}
            \item Use the J drive on one of the Department's computers, or your preferred location on your own computer
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Scripts}
    \begin{itemize}[<+- | alert@+>]
        \item The window with your code in it should say ``name.py'' in the title bar
        \item The window you started with should say ``Python <version> Shell''
        \item Also, the program window should have a Run pull-down menu, and the shell window should not
        \item In the program window, choose Run | Run Module, or just press F5, to run this program
        \item Notice that your program runs in the shell window (you may have to click in it to activate it)
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Scripts}
    \begin{itemize}[<+- | alert@+>]
        \item Running name.py in this way treats the file like a \textit{script}
        \item The variables from the script are still available in the shell window after the script is done running
        \item Type \mintinline{python}{print(name)} right now in the shell window to confirm that's true
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Modules}
    \begin{itemize}[<+- | alert@+>]
        \item Let's design a module to help us remember some world capitals
        \item Open a new file
        \item It's a good idea to save your new file right away before even typing any code in
        \begin{itemize}[<+- | alert@+>]
            \item This way you can just press Control-S (Command-S on a Mac) to save again later
        \end{itemize}
        \item Let's call this file \texttt{capitals.py}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Modules}
    \begin{itemize}[<+- | alert@+>]
        \item Type in this code (or copy and paste from a file your instructor has provided you):\\
        \inputminted{py}{capitals.py}
        \item Then, in \texttt{name.py} type this:\\
        \mintinline{python}{import capitals}
        \item Make sure both files are saved
        \item Now run \texttt{name.py} again
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Modules}
    \begin{itemize}[<+- | alert@+>]
        \item Nothing seems to be different
        \item The \texttt{name} variable is still available, like last time; run \mintinline{python}{print(name)} to confirm
        \item What about the capitals?  Run \mintinline{python}{print(iceland)}
        \item The capitals are stored, but we can't access them like that
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Modules}
    \begin{itemize}[<+- | alert@+>]
        \item Remember, the capitals were \textit{imported}
        \item That means \texttt{capitals} is a \textit{module} now
        \item We must access a module's variables by typing their full names -- the module name, then a \textit{dot operator} (it's really just a period), and then the variable name
        \item Run \mintinline{python}{print(capitals.iceland)}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Modules}
    \begin{itemize}[<+- | alert@+>]
        \item This is also true in the script that imports the module
        \item Add this to the end of the name module:\\
        \mintinline{python}{print("The capital of Iceland is", iceland)}
        \item Run the module (IDLE will prompt you to save if you haven't) and you will get the same error as before
        \item Change the line to print \texttt{capitals.iceland} and it will work
        \item You could also write a line like this to use \texttt{iceland} without qualifying the module:\\
        \mintinline{python}{iceland = capitals.iceland}\\
        \item Use this sparingly; it's a lot to type, and it also makes it less clear where the variable comes from
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Modules}
    \begin{itemize}[<+- | alert@+>]
        \item Python is an \textit{open source} programming language
        \item In open source culture, reputation is earned by giving things away
        \item Open Source's \alert{\textit{gift culture}} has led to a \textit{lot} of Python code out there available for us to use
        \item Some modules, like the \texttt{math} module, are included when installing Python
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Modules}
    \begin{itemize}[<+- | alert@+>]
        \item When you \mintinline{python}{import math} at the top of your script, you can call its functions to perform interesting calculations
        \item There are many exponential and trigonometric functions in the \texttt{math} module
        \item Try \mintinline{python}{math.pow(3, 2)}
        \item Or \mintinline{python}{math.cos(math.pi)}
        \item (Notice that \mintinline{python}{math.pi} is a variable, not a function; don't use parentheses)
        \item There is a list of functions and variables available in the \texttt{math} module in zyBooks
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Homework}
    \begin{itemize}
        \item Complete the \alert{Challenge Activities} in zyBooks based on today's class, and the \alert{Participation Activities} that will prepare you for the next class 
    \end{itemize}
\end{frame}