from graphics import *
win = GraphWin('Circles!', 300, 200)
points = []
circles = []
for i in range(6):
    p = Point(100 + (i * 20), 100)
    points.append(p)

for point in points:
    circle = Circle(point, 50)
    circles.append(circle)

for circle in circles:
    circle.draw(win)

win.getMouse()
win.close()
