#! /usr/bin/env python3

prompt = 'Enter a number between 1 and 10: '
user_value = int(input(prompt))
if user_value < 1 or user_value > 10:
    user_value = int(input(prompt))

print('You entered', user_value)
