def add_one(x):
    return x + 1

print(add_one(5))
print(add_one(-12))
x = add_one(34)
x = add_one(x)
print(x)
