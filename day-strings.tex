% !TEX root = lecture.tex

\section{Day \thesection: Strings and Numeric Types, and Writing a First Script}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I need to represent a word or a phrase in Python}
    \tools{IDLE}
    \newtools{Strings, numbers, shebangs, docstrings}
    How do I work with sequences of characters?
\end{frame}

\begin{frame}{Day \thesection\ Strings}
    \begin{itemize}[<+- | alert@+>]
        \item A \alert{\textit{character}} is a letter, or a digit, or a piece of punctuation -- something that generally requires one keystroke to generate
        \item You learned at the end of Chapter 2 that some characters like the newline character require two keystrokes -- these are called \alert{\textit{escape sequences}} or \alert{\textit{digraphs}}
        \begin{itemize}[<+- | alert@+>]
            \item \textbf{di} (Greek): two
            \item \textit{graph} (Greek): to write
            \item \textbf{\textit{digraph}}: write two things
        \end{itemize}
        \item A \alert{\textit{string}} is a sequence of characters
        \item Each character has a numeric position, or \textit{index}, within the string
        \item Sequence indices always start at zero
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ String Literals}
    \begin{itemize}[<+- | alert@+>]
        \item A \alert{\textit{string literal}} is a string within either single or double quotes
        \item \mintinline{python}{'Hello world!'} is a string literal, and so is \mintinline{python}{"Hello world!"}
        \item A string literal can contain any characters, so \mintinline{python}{'65'} is a string literal, and so is \mintinline{python}{"Here comes a new line\n"}
        \item A single character within quotes like \mintinline{python}{'x'} is still a string literal
        \item Type \mintinline{python}{''} to create a string literal that represents the \textit{empty string}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ String Variables}
    \begin{itemize}[<+- | alert@+>]
        \item We can use a string literal to create or change the value of a string variable:\\
        \mintinline{python}{message = 'Hello World'}
        \item A string variable can also get a value from a function like the input function:\\
        \mintinline{python}{name = input('Type your name: ')}
        \item Note the use of a string literal as the prompt for the input function
        \item We can call the input function by passing it a variable instead of a literal:\\
        \mintinline{python}{prompt = 'Type your name: '}
        \mintinline{python}{name = input(prompt)}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Analyzing Strings}
    \begin{itemize}[<+- | alert@+>]
        \item Python can tell us quite a bit about a string
        \item Use the \texttt{len} function to determine how many characters a string contains
        \item Type the following into IDLE:
        \item \mintinline{python}{len('abc')}
        \item \mintinline{python}{len('abc\n')}
        \item \mintinline{python}{message = 'Hello World!'}
        \item \mintinline{python}{len(message)}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Analyzing Strings}
    \begin{itemize}[<+- | alert@+>]
        \item We can compose a string using the concatenation operator \texttt{+} and ask its length:
        \item \mintinline{python}{len('abc' + 'def')}
        \item \mintinline{python}{len(message + 'xyz')}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Analyzing Strings}
    \begin{itemize}[<+- | alert@+>]
        \item Using a number in square brackets after a string, we can access an individual character
        \item \mintinline{python}{message[ 0 ]}
        \item \mintinline{python}{message[ 11 ]}
        \item You will get an \texttt{IndexError} if you use an index that is outside of the string's range
        \item \mintinline{python}{message[ 12 ]}
        \item Use a negative index to start counting from the end of the string
        \item \mintinline{python}{message[ -1 ]}
        \item \mintinline{python}{message[ -2 ]}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Strings Are Immutable}
    \begin{itemize}[<+- | alert@+>]
        \item In Python, strings are \alert{\textit{immutable}}
        \item Changing the value of a string variable really means deleting the old one and creating a new replacement
        \item An assignment like \mintinline{python}{s3 = s1 + s2} doesn't change \texttt{s1} or \texttt{s2}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Mixed-Type Arithmetic}
    \begin{itemize}[<+- | alert@+>]
        \item Recall that there are two kinds of numbers in Python, floats and ints
        \item A float stores more information about a number than an int does
        \item When we perform arithmetic that mixes the two, the result will be a float
        \item \mintinline{python}{type(6 + 3)} should report that the result is of type \texttt{int}
        \item \mintinline{python}{type(6 + 3.2)} should report that the result is of type \texttt{float}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Mixed-Type Arithmetic}
    \begin{itemize}[<+- | alert@+>]
        \item Python automatically \textit{promotes} ints to floats when necessary so that information is not lost
        \item This is a temporary change
        \item \mintinline{python}{x = 6}
        \item \mintinline{python}{y = 3.2}
        \item \mintinline{python}{type(x + y)}
        \item \mintinline{python}{type(x)}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Mixed-Type Arithmetic}
    \begin{itemize}[<+- | alert@+>]
        \item We can force a number to be treated a certain way using the \mintinline{python}{int()} and \mintinline{python}{float()} functions
        \item We have already used the \mintinline{python}{int()} and \mintinline{python}{float()} functions when converting user input (which is always a string) to a numeric type
        \item Remember that the \mintinline{python}{int()} function \textbf{\textit{does not round}} its input; it \textit{truncates} it
        \item \mintinline{python}{int(5.999999999999)}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Writing a Python Script}
    \begin{itemize}[<+- | alert@+>]
        \item Your instructor will be assigning a new kind of homework today
        \item Remember what you read about a properly formatted script header
        \item Complete the assignment by the due date
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Homework}
    \begin{itemize}
        \item Keep working on the \alert{Name Processing Homework} if you haven't already completed it
        \item Complete the \alert{Challenge Activities} in zyBooks based on today's class, and the \alert{Participation Activities} that will prepare you for the next class 
    \end{itemize}
\end{frame}