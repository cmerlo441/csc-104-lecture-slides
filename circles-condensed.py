from graphics import *
win = GraphWin('Circles!', 300, 200)
circles = []
for i in range(6):
    p = Point(100 + (i * 20), 100)
    circles.append(Circle(p, 50))

for circle in circles:
    circle.draw(win)

win.getMouse()
win.close()
