#! /usr/bin/env python3

sum = 0
prompt = 'Enter a positive number: '
user_value = int(input(prompt))
while user_value > 0:
    sum += user_value
    user_value = int(input(prompt))

print('The sum is', sum)
