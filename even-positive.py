#! /usr/bin/env python3
'''Display characteristics of a number.

If a given number is even, say so.
If a given number is positive, say so.
'''

number = int(input('Type in a number: '))
if number % 2 == 0:
    print('That number is even.')

if number > 0:
    print('That number is positive.')
