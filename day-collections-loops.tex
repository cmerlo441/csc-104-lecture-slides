% !TEX root = lecture.tex

\section{Day \thesection: Finite Loops and Collections}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I need to store and process a bunch of related information}
    \tools{Variables, loops}
    \newtools{Collections}
    How do I work with a bunch of data at once?
\end{frame}

\begin{frame}{Day \thesection\ Collections}
    \begin{itemize}[<+- | alert@+>]
        \item In general terms, a \textbf{\alert{collection}} is a variable that contains many data values
        \item The data values in a collection are usually related to each other in some way, frequently by data type or by usefulness
        \item One particular kind of collection that is useful to understand across programming languages and across problem types is the \textbf{\alert{array}}
        \item An array collects many pieces of data of the same type
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections and Indices}
    \begin{itemize}[<+- | alert@+>]
        \item Most languages utilize the \textit{square brackets} \textbf{\alert{[]}} to create and work with arrays
        \item We can create an array of important integers like this:\\
        \mintinline{py}{numbers = [37, 14, 41, 31]}
        \item This array now has four \textit{elements} or \textit{members}
        \item Like strings, each element of the array has an \textit{index}, which numerically indicates the element's position within the array
        \item What will this code do?\\
        \mintinline{py}{print(numbers[1])}
        \item Remember that the 37 is in position \textit{zero}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections and Loops}
    \begin{itemize}[<+- | alert@+>]
        \item If we want to print all of the data in \texttt{numbers}, it seems logical to use a loop to do so
        \item In fact, the relationship between arrays (or collections of any kind) and loops is a very strong one
        \item Most collection processing happens with a loop
        \item There are, as you might have guessed, several ways to loop over this array of numbers and display them all
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections and While Loops}
    \begin{itemize}[<+- | alert@+>]
        \item Since we've learned about while loops already, knowing what we know about an array's indices, we could write code like this:
        \item \inputminted{py}{array-while-loop.py}
        \item This works just fine, but it isn't as generically useful as we would like
        \item What happens if we add a value to \texttt{numbers}?
        \item What happens if we \textit{remove} a couple of values from \texttt{numbers}?
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections and While Loops}
    \begin{itemize}[<+- | alert@+>]
        \item There will be times when we either don't know, or can't predict, how many values will be in an array
        \item We learned about the \mintinline{py}{len()} function when we discussed strings
        \item Thankfully, that function is useful on just about any collection in Python
        \item This code will work no matter how many values you add to or remove from \texttt{numbers}:\\
        \inputminted{py}{array-while-loop-len.py}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections and For Loops}
    \begin{itemize}[<+- | alert@+>]
        \item The code on the previous slide is not the most \textit{pythonic} way to loop over an array
        \item There is a different kind of loop in Python that makes the task of looping over a collection much easier to type, and makes the code much easier to read
        \item This kind of loop is called the \textbf{\alert{for loop}}, because it starts with the Python reserved word \mintinline{py}{for}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections and For Loops}
    \begin{itemize}[<+- | alert@+>]
        \item When writing a \mintinline{py}{for} loop to loop over a Python collection, we first come up with a variable name we can use to refer to \textit{one} of the members of the collection
        \item It's a good idea to name a collection with a plural noun, so the singular of that plural is usually recommended
        \item In this example, since we're looping over \texttt{numbers}, a good choice for this new variable is \texttt{number}
        \item We then write the loop so that in each iteration, \texttt{number} refers to the next element in the array, like this:
        \item \mintinline{py}{for number in numbers:}\\
        \texttt{\phantom{xxxx}}\textit{<body of the loop>}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections and For Loops}
    \begin{itemize}[<+- | alert@+>]
        \item Now displaying all the numbers is as easy as this:
        \item \inputminted{py}{array-for-loop.py}
        \item Other sort of array processing then becomes fairly easy as a result
        \item See if you can write code that displays the \textit{sum} of these numbers
        \item If you have that done, see if you can write code that displays the \textit{mean} of the numbers
        \item If you've done \textit{that}, see if your code still works when you change how many values \texttt{numbers} contains
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections of Non-Numeric Items}
    \begin{itemize}[<+- | alert@+>]
        \item Note that numbers are not the only things that a collection can collect
        \item \inputminted{py}{dwarfs.py}
        \item You can even collect objects.  See if you can determine what this code does:
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections of Non-Numeric Items}
    \begin{itemize}[<+- | alert@+>]
        \inputminted[fontsize=\scriptsize]{py}{circles-before-loop.py}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections of Non-Numeric Items}
    \begin{itemize}[<+- | alert@+>]
        \item With a couple of new Python tools, we can make that code much shorter
        \item The \mintinline{py}{range(n)} function creates a \textit{range object}, which is capable of generating a sequence of integers starting with 0 and ending before $n$
        \item Type this:\\
        \mintinline{py}{list(range(10))}
        \item You can also loop over a range:\\
        \inputminted{py}{loop-over-range.py}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Collections of Non-Numeric Items}
    \begin{itemize}[<+- | alert@+>]
        \item The \mintinline{py}{append()} function allows us to add an item to the end of an array\\
        \item \inputminted{py}{append.py}
        \item If you need to, you can create an empty sequence with code like \mintinline{py}{sequence = []} and just append items to it
        \item With these tools, now we can draw our circles with much less code:
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Drawing Circles Without Loops and Collections}
    \begin{itemize}[<+- | alert@+>]
        \inputminted[fontsize=\scriptsize]{py}{circles-before-loop.py}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Drawing Circles With Loops and Collections}
    \begin{itemize}[<+- | alert@+>]
        \inputminted[fontsize=\scriptsize]{py}{circles-after-loop.py}
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Homework}
    \begin{itemize}
        \item Complete the \alert{Partcipation Activities} from Chapter 7 ``Data Collections'' and Chapter 8 ``For Loops''
    \end{itemize}
\end{frame}