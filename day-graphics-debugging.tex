% !TEX root = lecture.tex

\section{Day \thesection: When Graphics Goes Wrong!}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I need to make sure my graphical program works}
    \tools{All the skills you've developed so far}
    \newtools{Code tracing strategies}
    Does this graphical program work?  If not, can I fix it?
\end{frame}

\begin{frame}{Day \thesection\ What Kind of Things Can Go Wrong?}
    \begin{itemize}[<+- | alert@+>]
        \item In the ``Colorful Circles'' homework that you completed for today, you asked the user to enter information about two colors
        \item Recall that valid RGB values must be between 0 and 255
        \item What do you think should happen if the person using your program typed in 300, 300, and 300 for one of the colors?
        \item Run your program now and type those values in
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ What Kind of Things Can Go Wrong?}
    \begin{itemize}[<+- | alert@+>]
        \item Is that what you expected?
        \item Just because a program runs doesn't mean it ``works''
        \item How can we improve the user experience?
        % \item 
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ How Do We Know Whether Something Went Wrong?}
    \begin{itemize}[<+- | alert@+>]
        \item When a user types a value in for our program to read, two things can happen
        \begin{itemize}[<+- | alert@+>]
            \item Outcome 1: the value can be something we expected
            \item Outcome 2: the value can be something we didn't expect
        \end{itemize}
        \item Didn't we learn how to deal with two possible outcomes a few weeks ago?
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The If Statement in Python}
    \begin{itemize}[<+- | alert@+>]
        \item An \mintinline{py}{if} statement in Python is very much like the statements we wrote in the first third of the class
        \item Review: when writing an if statement, we need to determine what \textbf{\alert{variable}} represents the value we're examining
        \item We need to compare the variable's \textbf{\alert{present state}} to some \textbf{\alert{possible value}}
        \item This comparison will lead us to one or two \textbf{\alert{outcomes}}
        \item The amount of outcomes drives the amount of questions we ask
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The If Statement in Python}
    \begin{itemize}%[<+- | alert@+>]
        \item Recall the general syntax of a simple if statement:\\
        \texttt{if \textit{condition}:}\\
        \texttt{\phantom{xxxx}outcome$_T$}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Determining Whether User Input is Correct}
    \begin{itemize}[<+- | alert@+>]
        \item Problem: The user has typed in a value and we want to know if it's acceptable
        \item Possible outcomes:
        \begin{itemize}[<+- | alert@+>]
            \item It is
            \item It's too high
            \item It's too low
        \end{itemize}
        \item What needs to be compared?
        \begin{itemize}[<+- | alert@+>]
            \item What the user typed in, and the upper and lower bounds of acceptable values
        \end{itemize}
        \item How do we do the comparison?
        \begin{itemize}[<+- | alert@+>]
            \item Compare the \textbf{present state} (what the user typed in) with a \textbf{possible value} (what the bounds are)
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The If Statement in Python}
    \begin{itemize}[<+- | alert@+>]
        \item We're going to start simply
        \item If your code looks like this:\\
        \mintinline{py}{red = int(input('Enter a value for red: '))}
        \item Try to write an if statement that displays a message if the value the user typed in is too high
        \item Hopefully your code looks like this:
        \item \inputminted{python}{first-if.py}
        \item The indenting on the second line is \textbf{\alert{required}}; if your program isn't working, first make sure the print statement is indented properly
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The If Statement in Python}
    \begin{itemize}[<+- | alert@+>]
        \item The code from the previous slide is a good start
        \item Remember that you can put a pound sign at the start of a line to turn it into a \textbf{comment}
        \item \texttt{\# This is a comment}
        \begin{itemize}[<+- | alert@+>]
            \item Yes, it's also called a hashtag
            \item Or a number sign
            \item Or an \textit{octothorpe} (yes, really)
        \end{itemize}
        \item \textit{Comment out} the if statement from the previous slide, so that it's still there but won't run
        \begin{itemize}[<+- | alert@+>]
            \item The if statement is two lines long; comment both out
        \end{itemize}
        \item Now write an if statement that displays the message ``That value is invalid.'' if the user's input is invalid in any way
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The If Statement in Python}
    \begin{itemize}[<+- | alert@+>]
        \item Remember, with only one outcome, you only get to say ``if'' once
        \item This is a job for a \textit{complex conditional!}
        \item Code like this will do it:
        \item \inputminted{py}{second-if.py}
        \item Should we do anything else?
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The If Statement in Python}
    \begin{itemize}[<+- | alert@+>]
        \item Maybe you've decided we should end the program if the input is invalid
        \item Use the command \mintinline{py}{sys.exit(message)} to quit
        \item It is important that you indent the entire outcome
        \item Look carefully at the next two code examples
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The If Statement in Python}
    \begin{itemize}%[<+- | alert@+>]
        \item There's a big difference between this\dots\\
        \inputminted{py}{exit-correctly.py}
        \item \dots and this\dots\\
        \inputminted{py}{exit-incorrectly.py}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The If Statement in Python}
    \begin{itemize}%[<+- | alert@+>]
        \item It's a good idea to leave a blank line after the end of your if statement so that you can clearly see where it ends
        \inputminted{py}{exit-with-space.py}
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Get Ready for Exam 2!}
    \begin{itemize}
        \item Day 19 is Exam 2 Review Day
        \item Use the time before Day 19 starts to review what we've done in Python
        \item Make sure you can write the code that you've been assigned to write so far, and that you can trace all the code you've seen
        \item There will be \alert{Participation Activities} assigned for Day 21
    \end{itemize}
\end{frame}