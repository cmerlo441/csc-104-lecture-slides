% !TEX root = lecture.tex

\section{Day \thesection: Introduction to Python}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I want to tell a computer to follow my commands}
    \tools{Curiosity, perseverance, a computer}
    \newtools{The Python Programming Language, The IDLE Python Programming Environment}
    How do I write a computer program?
\end{frame}

\begin{frame}{Day \thesection\ The ``Hello World'' Program}
\begin{itemize}[<+- | alert@+>]
    \item It is tradition when learning a new programming language to write a program that displays the phrase ``Hello World!'' on the screen
    \item This program is very complicated in a lot of programming languages
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ The ``Hello World'' Program}
    \begin{itemize}[<+- | alert@+>]
        \item In the early days of programming, there was Assembly Language:
        
        \begin{minted}{nasm}
mov ax,cs
mov ds,ax
mov ah,9
mov dx, offset Hello
int 21h
xor ax,ax
int 21h

Hello:
  db "Hello World!",13,10,"$"
        \end{minted}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ The ``Hello World'' Program}
    \begin{itemize}[<+- | alert@+>]
        \item The C language came along and made things a little better:
        
        \begin{minted}{c}
#include <stdio.h>
int main( char **argv, int argc ) {
    printf( "%s\n", "Hello World!" );
    return 0;
}
        \end{minted}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ The ``Hello World'' Program}
    \begin{itemize}[<+- | alert@+>]
        \item Computer Science and Information Technology majors will learn Java sometime after this course:
        
        \begin{minted}{java}
public class HelloWorld {
    public static void main( String args[] ) {
        String message =
            new String( "Hello World!" );
        System.out.println( message );
    }
}
        \end{minted}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ The ``Hello World'' Program}
    \begin{itemize}[<+- | alert@+>]
        \item Thankfully, we aren't learning any of those languages in this class
        \item Here's the Hello World program in Python:
        
        \begin{minted}{py}
print("Hello, World!")
        \end{minted}
        \item That's it.  That's the whole program.  Just that one line.
        \item This is just one of several reasons why the Python language has become so popular
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ The ``Hello World'' Program}
    \begin{itemize}[<+- | alert@+>]
        \item One of the tools that comes with Python is a program called \alert{IDLE} that we can use to type and run our programs
        \item Instructions and advice for downloading and installing Idle are available in the \alert{Lecture Notes}
        \item If you are using the College's computer in front of you, click on the box at the bottom left of the screen, which says ``Type here to search'', type in \texttt{idle}, and click on the application's name in the box that pops up
        \item Mac and Linux instructions are in Appendix A of the Lecture Notes
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ The ``Hello World'' Program}
    \begin{itemize}[<+- | alert@+>]
        \item When IDLE starts up, it shows some information at the top of the window about the current version of Python
            \begin{itemize}
                \item Something like:\\ {\footnotesize \texttt{Python \alert<3-4>{3}.7.2 (default, Jan 13 2019, 12:50:01)}}
            \end{itemize}
        \item Make sure the version number at the top of the window starts with 3
            \begin{itemize}[<+- | alert@+>]
                \item Much of the code we will be writing this semester \textit{will not work} if IDLE is using version 2
            \end{itemize}
        \item Your prompt looks like this: \texttt{>>>}
        \item Type in the Python program we want to run and press Enter at the end of the line
            \begin{itemize}[<+- | alert@+>]
                \item \mintinline{python}{print("Hello World!")}
                \item Note that single quotes also work, so you could have typed \mintinline{python}{print('Hello World!')} instead
            \end{itemize}
        \item IDLE should respond with the message 'Hello World!'
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ The ``Hello World'' Program}
    \begin{itemize}[<+- | alert@+>]
        \item Congratulations!  You are a Python programmer!
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Personalizing the ``Hello World'' Program}
    \begin{itemize}[<+- | alert@+>]
        \item Now write a program that displays a personalized message to you, like ``Hello Olivia!''
        \item You should have typed something like \mintinline{python}{print("Hello Olivia!")}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Writing an Interactive Program}
    \begin{itemize}[<+- | alert@+>]
        \item This program won't be much fun to show your friends, unless you all have the same first name
        \item This program would be more interesting if it were \textit{interactive}
        \item We will need to use two new Python tools to ask the program's user what her name is and then personalize a greeting
        \item First, let's learn about variables
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Writing an Interactive Program}
    \begin{itemize}[<+- | alert@+>]
        \item A \textbf{\alert{variable}} is a tool that lets us store information for later use
        \item There are rules that govern what names we can give to variables, which we will explore in Chapter 2
        \item We're going to create a variable called \texttt{name} that can store a person's first name
        \item When you create this variable, \textit{initialize} it with your own name
        \item Olivia should type:\\ \mintinline{python}{name = 'Olivia'}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Writing an Interactive Program}
    \begin{itemize}[<+- | alert@+>]
        \item Once data is stored in a variable, we can use the print command (technically, the print \textit{function}) to display the data
        \item Just use the variable name within the parentheses \textit{without quotes}, like this:\\ \mintinline{python}{print(name)}
        \item When you run this program, you should see your name in the output, just like before; but this time, your name was retrieved from memory to be displayed
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Writing an Interactive Program}
    \begin{itemize}[<+- | alert@+>]
        \item How do we make the program say ``Hello Olivia!'' instead of just ``Olivia''?
        \item We can use the comma operator to print many items in one line
        \item The comma operator automatically adds a space between each pair of items
        \item Change the print statement you wrote before so that it looks like this:\\ \mintinline{python}{print('Hello', name)}
        \item Change it again to get the exclamation point at the end
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Two Kinds of Concatenation}
    \begin{itemize}[<+- | alert@+>]
        \item Did you get ``Hello Olivia !'' with an extra space before the exclamation point?
        \item There are two ways to \textit{\alert{concatenate}} strings, or combine strings into one long one
        \item You've used one -- the comma concatenates strings together and places a space in between
        \item The other way is to use a plus sign -- this concatenates two strings \textit{without} inserting a space
        \item Change your print statement to get ``Hello Olivia!'' by using a plus sign in the right place
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Writing an Interactive Program}
    \begin{itemize}[<+- | alert@+>]
        \item In that program, we \textit{\alert{hard-coded}} the value of the \texttt{name} variable
        \item Every time that program runs it will say ``Hello Olivia!'' until we change the source code
        \item We can make this program \textit{\alert{interactive}} by using the \texttt{input()} function
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Writing an Interactive Program}
    \begin{itemize}[<+- | alert@+>]
        \item Just type this and press the Enter key, and see if you can determine what your program is doing:\\ \mintinline{python}{input()}
        \item If you just hit the Enter key again, you will see two quote marks
        \item But (type \mintinline{python}{input()} again) if you type something before pressing Enter the second time, what you type will be \textit{echoed} back to you inside those quote marks
        \item That text is the \textit{return value} from the input function
        \item That's useful -- we can capture that return value in a variable
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Writing an Interactive Program}
    \begin{itemize}[<+- | alert@+>]
        \item Type these two lines (you will have to type in some input after the first one):
\begin{minted}{python}
name = input()
print('Hello', name + '!')
\end{minted}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Writing an Interactive Program}
    \begin{itemize}[<+- | alert@+>]
        \item We're almost there!  But this program doesn't tell the user what to do
        \item An interactive program like this needs a \textbf{\alert{prompt}}
        \item You can write a prompt between the parentheses when using the \texttt{input} function
        \item Try this:\\ \mintinline{python}{input("Hi!  What's your name?")}
        \item What do you notice?
        \item To add space between the prompt and what gets typed, add that space \textit{inside} the quotes:\\ \mintinline{python}{input("Hi!  What's your name? ")}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Arithmetic}
    \begin{itemize}[<+- | alert@+>]
        \item It's easy to do a little bit of arithmetic in Python
        \item \mintinline{python}{print(3 + 2)}
        \item \mintinline{python}{print(3 - 2)}
        \item \mintinline{python}{print(3 * 2)}
        \item \mintinline{python}{print(3 / 2)}
        \begin{itemize}[<+- | alert@+>]
            \item \textbf{Note:} If you got an answer of 1 instead of 1.5, you are using the \textbf{\textit{wrong version}} of Python, and many things we will discuss in the future won't work for you.
        \end{itemize}
        \item \mintinline{python}{print(3 ** 2)}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Numerical Input}
    \begin{itemize}[<+- | alert@+>]
        \item Run this program and look \textit{carefully} at the output:
        \item \mintinline{python}{input('Type in a number: ')}
        \item Did the program return a number to you?
        \item Look again at the quote marks around your number
    \end{itemize}
\end{frame}

{ % all template changes are local to this group.
    \setbeamertemplate{navigation symbols}{}
    \begin{frame}[plain]
        \begin{tikzpicture}[remember picture,overlay]
            \node[at=(current page.center)] {
                \includegraphics[keepaspectratio,
                                 width=\paperwidth,
                                 height=\paperheight]{itsatrap}
            };
        \end{tikzpicture}
     \end{frame}
}

\begin{frame}[fragile]{Day \thesection\ Numerical Input}
    \begin{itemize}[<+- | alert@+>]
        \item What happens if we try to use that value in a mathematical expression?
        \item \begin{minted}{python}
my_number = input('Type in a number: ')
print(my_number * 3)
        \end{minted}
        \item There are different \alert{types} of data in a computer program
        \item The \mintinline{python}{input()} function can only read in a \alert{string} (``\texttt{str}'')
        \item Python treated your input like a string, not a number, and the \texttt{*} operator causes a string to be displayed many times
        \item We want our input to be treated as an \alert{integer} (``\texttt{int}'')
        \item This is where the \mintinline{python}{int()} function comes in handy
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Day \thesection\ Numerical Input}
    \begin{itemize}[<+- | alert@+>]
        \item Instead of \mintinline{python}{input('Type in a number: ')}\\ try this:\\ \mintinline{python}{int(input('Type in a number: '))}
        \item The \mintinline{python}{int()} function converts the string input into an integer
        \item You should see something like \texttt{4} instead of \texttt{'4'}
        \item Now try:\\ \mintinline{python}{my_number = int(input('Type in a number: '))}
        \item This stores a number in \texttt{my\_number} instead of a string
        \item When you \mintinline{python}{print(my_number * 3)} it should work
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Homework}
    \begin{itemize}
        \item If you have a computer at home, or at work, or at a friend's house, where you plan to do homework, perform all these steps on that computer, so that you know Python works there
        \item Complete the \alert{Challenge Activities} in zyBooks based on today's class, and the \alert{Participation Activities} that will prepare you for the next class 
    \end{itemize}
\end{frame}