% !TEX root = lecture.tex

\section{Day \thesection: While Loops}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I have Python code that I want to run many times in a row}
    \tools{Python code}
    \newtools{While loops}
    How do I make a task happen over and over again?
\end{frame}

\begin{frame}{Day \thesection\ Loops}
    \begin{itemize}[<+- | alert@+>]
        \item All semester, we have discussed the idea that there are three basic kinds of statements in any computer program
        \item \textbf{\alert{Sequential statements}} run in the order they appear in the source code
        \item \textbf{\alert{Selection statements}} (or \textit{if statements}) run one branch or another based upon a condition
        \item It's time to discuss the third kind of statement
        \item \textbf{\alert{Iterative statements}} (or \textit{loops}) run a portion of code many times in a row
        \item Programmers use the verb \textit{iterate} and the noun \textit{iteration} when describing loops
        \item ``That loop iterated five times.''  ``This will perform seven iterations.''
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ While Loops}
    \begin{itemize}[<+- | alert@+>]
        \item A \textbf{\alert{loop}} in any programming language is a statement that runs a series of statements many times in a row
        \item In particular, a \textbf{\alert{while loop}} in Python runs a series of statements as long as some \textit{condition} is true
        \item The syntax for a while loop is very similar to that of a simple \texttt{if} statement:\\
        \inputminted{py}{first-while.py}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ While Loops}
    \begin{itemize}[<+- | alert@+>]
        \item<1-> \inputminted{py}{first-while.py}
        \item The \textbf{\alert{body}} of this loop -- the code that's indented -- will run first when \mintinline{py}{x} is equal to 1
        \item Then \mintinline{py}{x} gets changed to 2, and the condition \mintinline{py}{x < 5} is evaluated again
        \item Since the condition is still true, the body runs again while \mintinline{py}{x} is 2
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ While Loops}
    \begin{itemize}[<+- | alert@+>]
        \item<1-> \inputminted{py}{first-while.py}
        \item Then the body runs when \mintinline{py}{x} is 3, and then it runs again when \mintinline{py}{x} is 4
        \item After the fourth time that \mintinline{py}{'Hello'} is printed, the value of \mintinline{py}{x} becomes 5
        \item Now the condition is false, and so the loop stops looping
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        Sometimes programmers find it easier to trace a loop like this using a table, like the one you filled in on Exam 2
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                \alert{1} & ~ & ~\\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        Sometimes programmers find it easier to trace a loop like this using a table, like the one you filled in on Exam 2
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & \alert{True} & ~\\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        Sometimes programmers find it easier to trace a loop like this using a table, like the one you filled in on Exam 2
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & \alert{Hello}\\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                \alert{2} & ~ & ~ \\
                ~ & ~ & ~ \\
                ~ & ~ & ~ \\
                ~ & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & \alert{True} & ~ \\
                ~ & ~ & ~ \\
                ~ & ~ & ~ \\
                ~ & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & True & \alert{Hello} \\
                ~ & ~ & ~ \\
                ~ & ~ & ~ \\
                ~ & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & True & Hello\\
                \alert{3} & ~ & ~ \\
                ~ & ~ & ~ \\
                ~ & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & True & Hello\\
                3 & \alert{True} & ~ \\
                ~ & ~ & ~ \\
                ~ & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & True & Hello\\
                3 & True & \alert{Hello}\\
                ~ & ~ & ~ \\
                ~ & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & True & Hello\\
                3 & True & Hello\\
                \alert{4} & ~ & ~ \\
                ~ & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & True & Hello\\
                3 & True & Hello\\
                4 & \alert{True} & ~ \\
                ~ & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & True & Hello\\
                3 & True & Hello\\
                4 & True & \alert{Hello} \\
                ~ & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & True & Hello\\
                3 & True & Hello\\
                4 & True & Hello\\
                \alert{5} & ~ & \phantom{\textit{<nothing happens>}} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Tracing a While Loop}
    \begin{itemize}
        \inputminted{py}{first-while.py}
        \setcounter{table}{0}
        \begin{table}
            \caption{Tracing a While Loop}
            \begin{tabular}{c|c|c}
                \toprule
                \mintinline{py}{x} & \mintinline{py}{x < 5} & Output\\
                \midrule
                1 & True & Hello\\
                2 & True & Hello\\
                3 & True & Hello\\
                4 & True & Hello\\
                5 & \textbf{\alert{False}} & \textit{<nothing happens>} \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Finite and Indefinite Loops}
    \begin{itemize}[<+- | alert@+>]
        \item Some loops, like the one we just saw, are \textbf{\alert{finite}} -- we programmers can read the code and determine how many times the body will run
        \item Sometimes, however, we want a loop to iterate an \textit{unknown amount of times}, until some condition is met
        \item These loops are called \textbf{\alert{indefinite}} loops
        \item (A third category of loops, called \textit{infinite loops}, keep looping until some external influence causes the program to end)
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Indefinite Loops}
    \begin{itemize}[<+- | alert@+>]
        \item Let's say we've asked the user to enter an integer between 1 and 10
        \item We need this input before the program can continue
        \item We don't want to just \mintinline{py}{sys.exit()} if the input is no good; we want to ask the user to try again
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Indefinite Loops}
    \begin{itemize}[<+- | alert@+>]
        \item You might want to try to solve this problem with a conditional statement first, like this:
        \item \inputminted{py}{one-to-ten-just-if.py}
        \item But what happens if the user enters $-15$ first and 35 after that?
        \item \texttt{You entered 35} will be displayed 
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Indefinite Loops}
    \begin{itemize}[<+- | alert@+>]
        \item Let's change that \mintinline{py}{if} to a \mintinline{py}{while}, like this:
        \item \inputminted{py}{one-to-ten.py}
        \item Now what happens if the user enters $-15$ first and 35 after that?
        \item The program will keep asking for input until valid input is entered
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Indefinite Loops}
    \begin{itemize}[<+- | alert@+>]
        \item Note that the loop we just looked at is \textit{indefinite} -- you and I can't tell how many times it will iterate by reading it
        \item The amount of iterations is dependent upon the user's capacity to read and follow instructions
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Sentinel-Controlled Loops}
    \begin{itemize}[<+- | alert@+>]
        \item Sometimes you will want a loop to read and process many input values
        \item We can accomplish this by deciding on a \textbf{\alert{sentinel value}} that is not a valid data value, but serves to indicate that the end of data has been reached
        \item Imagine a program that asks the user to enter many positive values, and displays their sum
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Sentinel-Controlled Loops}
    \begin{itemize}[<+- | alert@+>]
        \item We will need a variable to store the sum of all the inputs
        \item We will create a prompt like\\
        \mintinline{py}{prompt = 'Enter a positive number: '}
        \item Ask for a first data value:\\
        \mintinline{py}{user_value = int(input(prompt))}
        \item Then, as long as the user's input is valid (positive), add it to the sum and ask for another value
        \item Take a minute now to see if you can plan this program, and then we will look at a solution together
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Sentinel-Controlled Loops}
    \inputminted{py}{input-sum.py}
\end{frame}

\begin{frame}{Day \thesection\ Things to Watch Out For}
    \begin{itemize}[<+- | alert@+>]
        \item There are some common mistakes programmers make when writing loops
        \item Watch for these things when writing your own loops, and when tracing loops someone else has written
        \item \textbf{\alert{Indentation:}} Make sure the entire body of the loop is indented uniformly under the loop's header (4 spaces is recommended)
        \item \textbf{\alert{Condition:}} Make sure the loop's condition will be true when you expect it to be, and will become false when you expect it to  -- it is easy to write a loop that never iterates, or a loop that iterates infinitely, by accident
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Things to Watch Out For}
    \begin{itemize}[<+- | alert@+>]
        \item \textbf{\alert{Update:}} Make sure the variable that controls the condition in your loop gets updated appropriately
        \item \textbf{\alert{Inputs and Prompts:}} For a sentinel-controlled loop, make sure the user knows what kind of input is valid and knows how to stop the loop
        \item \textbf{\alert{Sentinel Values:}} Make sure the loop actually stops when a sentinel value is entered
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Homework}
    \begin{itemize}
        \item Start working on \alert{Chapter 6 Challenge Activities}; they will be due two classes from now
        \item You will be assigned a programming activity next time, which will also be due two classes from now
    \end{itemize}
\end{frame}