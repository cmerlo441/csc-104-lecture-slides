from graphics import *
win = GraphWin('Circles!', 300, 200)
p1 = Point(100, 100)
p2 = Point(120, 100)
p3 = Point(140, 100)
p4 = Point(160, 100)
p5 = Point(180, 100)
p6 = Point(200, 100)

points = [p1, p2, p3, p4, p5]
circles = [Circle(p1, 50),
    Circle(p2, 50),
    Circle(p3, 50),
    Circle(p4, 50),
    Circle(p5, 50),
    Circle(p6, 50)
]
for circle in circles:
    circle.draw(win)

win.getMouse()
win.close()
