dwarfs = [
    'Doc', 'Grumpy', 'Happy', 'Sleepy',
    'Bashful', 'Sneezy', 'Dopey'
]
print('Which dwarf is your favorite?')
for dwarf in dwarfs:
    print(dwarf)
