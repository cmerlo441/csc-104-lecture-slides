% !TEX root = lecture.tex

\section{Day \thesection: Introduction to Graphics}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I need to draw some neat pictures}
    \tools{Python, IDLE}
    \newtools{The \texttt{graphics.py} Package}
    How do I make a fun-looking Python program?
\end{frame}

\begin{frame}{Day \thesection\ Classes and Objects}
    \begin{itemize}[<+- | alert@+>]
        \item The Lecture Notes for today explained that a \alert{class} defines the common behaviors and properties of \alert{instances} of that class
        \item We \alert{instantiate} a class by calling a function that is named after the class, like \mintinline{py}{my_pizza = Pizza()}
        \item Some classes let us (and others force us) to specify options in the parentheses, like \mintinline{py}{my_awesome_pizza = Pizza(pepperoni)}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The \texttt{graphics.py} Module}
    \begin{itemize}[<+- | alert@+>]
        \item Most of the classes we're going to want to use exist in a module which we must import
        \item The \texttt{graphics.py} module contains a bunch of classes that we will make a lot of use of
        \item Type \mintinline{py}{from graphics import *} to import all the classes and make it easy to use them
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The \texttt{GraphWin} Class}
    \begin{itemize}[<+- | alert@+>]
        \item A graphics library must enable us to create a window in which our application can run
        \item In Python, the application window is defined in a class called \texttt{GraphWin}
        \item We must instantiate the \texttt{GraphWin} class in order for an application to appear on the screen
        \item The \texttt{GraphWin()} function requires three \alert{arguments} or \alert{parameters}
        \begin{itemize}[<+- | alert@+>]
            \item First, a string to specify what text should appear in the window's title bar
            \item Second, how wide the window should be, in \alert{pixels}
            \item Lastly, how tall the window should be, also in pixels
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The \texttt{GraphWin} Class}
    \begin{itemize}[<+- | alert@+>]
        \item So, we can create a 250$\times$100 window that says ``An Empty Window'' in the titlebar by typing \mintinline{py}{win = GraphWin('An Empty Window', 250, 100)}
        \item If you typed that into IDLE's interactive window, you may be wondering why you don't see a new window
        \item The \mintinline{py}{GraphWin} object will disappear when the program ends, and unless we do something else, the program will end right away as soon as it starts
        \item Open a new file in IDLE, and save the new file as \texttt{empty-window.py}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The \texttt{GraphWin} Class}
    \begin{itemize}[<+- | alert@+>]
        \item Type this into \texttt{empty-window.py} to import the module:  \mintinline{py}{from graphics import *}
        \item Type this to create a window: \mintinline{py}{win = GraphWin('An Empty Window', 250, 100)}
        \item We can pass a message to the object called \texttt{win} using its name and the dot operator
        \item Let's ask \texttt{win} to wait for us to click the mouse inside the window, by typing \mintinline{py}{win.getMouse()}
        \item The \mintinline{py}{getMouse()} function tells us where in the window the mouse got clicked (we say that the function \textit{returns a \texttt{Point} object})
        \item We don't really care where the mouse gets clicked, but this keeps the window open long enough for us to see it
        \item Finally, let's ask \texttt{win} to shut down: \mintinline{py}{win.close()}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The \texttt{GraphWin} Class}
    This code:
    \inputminted{py}{empty-window.py}
    \bigskip
    Produces this window:\\

    \smallskip

    \includegraphics{empty-window}
\end{frame}

\begin{frame}{Day \thesection\ Computer Graphics}
    \begin{itemize}[<+- | alert@+>]
        \item Most of what you need to know about computer graphics is fairly straightforward
        \item Once a \mintinline{py}{GraphWin} object has been instantiated, we can think of the pixels within lying on a Cartesian plane like from geometry class
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Computer Graphics}
    \centering
    \includegraphics[scale=0.65]{cartesian}
\end{frame}

\begin{frame}{Day \thesection\ Computer Graphics}
    \begin{itemize}[<+- | alert@+>]
        \item<1-> Most of what you need to know about computer graphics is fairly straightforward
        \item<1-> Once a \mintinline{py}{GraphWin} object has been instantiated, we can think of the pixels within lying on a Cartesian plane like from geometry class
        \item However, the origin $(0,0)$ is at the top left, and all $y$ values are positive
        \item The lower left corner of \mintinline{py}{win} has the coordinate $(0,99)$, and the lower right corner has the coordinate $(249,99)$
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The \texttt{Point} Class}
    \begin{itemize}[<+- | alert@+>]
        \item The classes included in the \texttt{graphics.py} module help us draw interesting things on the screen
        \item These classes represent geometric objects, like lines and rectangles
        \item The simplest to explain is a \mintinline{py}{Point}
        \item A \mintinline{py}{Point} has an $x$ coordinate and a $y$ coordinate
        \item We instantiate a \mintinline{py}{Point} object by providing the $x$ and $y$ coordinates to the \mintinline{py}{Point()} function
        \item The code \mintinline{py}{p = Point(35, 15)} creates a \mintinline{py}{Point} object called \mintinline{py}{p} which is 35 pixels in from the left side of the window, and 15 pixels below the top of the window
        \item You won't see this point until you tell it to draw itself on the window like this: \mintinline{py}{p.draw(win)}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The \texttt{Point} Class}
    You should see this:\\

    \centering
    \includegraphics{point-35-15}
\end{frame}

\begin{frame}{Day \thesection\ The \texttt{Circle} Class}
    \begin{itemize}[<+- | alert@+>]
        \item The \texttt{Point} class is the basis upon which many other classes are built
        \item Before today's class you created a \mintinline{py}{Point}, and then created a \mintinline{py}{Circle} around that point:\\
        \mintinline{py}{p = Point(150, 150)}\\
        \mintinline{py}{c = Circle(p, 50)}
        \item This code created a circle whose center is at the point $(150,150)$ and whose radius is 50 pixels
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The RGB Color Model}
    \begin{itemize}[<+- | alert@+>]
        \item You also set two interesting properties of your \mintinline{py}{Circle} object using the \mintinline{py}{setOutline()} and \mintinline{py}{setFill()} functions
        \item \texttt{graphics.py} makes it easy to set these colors using strings such as \mintinline{py}{'red'}, but there are more options than that
        \item Computer colors are defined using the three primary colors of light: red, green, and blue
        \item Imagine you are in a completely dark room, sitting at a desk with three dimmer switches
        \item Each dimmer switch is attached to a colored light aimed at a white wall
    \end{itemize}
\end{frame}

\definecolor{mypurple}{RGB}{255, 0, 255}
\definecolor{mygray}{RGB}{127, 127, 127}
\definecolor{metsorange}{RGB}{252, 89, 16}
\begin{frame}{Day \thesection\ The RGB Color Model}
    \begin{itemize}[<+- | alert@+>]
        \item You can set each dimmer switch to a position between 0 (completely off) and 255 (as bright as possible)
        \item If you turn the red light's dimmer to 255, and leave the others alone, the wall will be \textcolor{red}{red}
        \item Leave the red light on, and turn the blue light to 255; this will make the wall \textcolor{mypurple}{purple}
        \item You can create interesting colors like \textcolor{metsorange}{Mets Orange} by setting the dials to 252, 89, and 16
        \item Turn all three dimmers to 127 (halfway on) for a pleasant shade of \textcolor{mygray}{gray}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ The RGB Color Model}
    \begin{itemize}[<+- | alert@+>]
        \item To use one of these colors instead of \mintinline{py}{'blue'} or \mintinline{py}{'green'}, use the \mintinline{py}{color_rgb()} function like this:\\
        \mintinline{py}{mets_orange = color_rgb(252, 89, 16)}\\
        \mintinline{py}{c.setFill(mets_orange)}
        \item You can also specify the color directly if you won't need it again:\\
        \mintinline{py}{c.setFill(color_rgb(255, 0, 255))}
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Homework}
    \begin{itemize}
        \item Complete the \alert{Colorful Circles} assignment
    \end{itemize}
\end{frame}
