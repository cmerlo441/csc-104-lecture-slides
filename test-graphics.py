#! /usr/bin/env python3

import graphics, math

width = 600
height = 300

center = graphics.Point(width / 2, height / 2)

win = graphics.GraphWin('This is a Test', width, height)

# center.draw(win)

# Make a collection of evenly-spaced points and draw some stuff

amount = 3

for coord in range(1, amount + 1):
    x = ( width / (amount + 1)) * coord
    p = graphics.Point(x, height / 2)
    c = graphics.Circle(p, min(width / 2, height / 2) - 1)
    c.setOutline('red')
    c.draw(win)


win.getMouse()
win.close()
