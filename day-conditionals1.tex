% !TEX root = lecture.tex

\section{Day \thesection: Introduction to Conditionals}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I want my computer to decide what to do}
    \tools{Pen, paper}
    \newtools{Relational operators, conditional statements}
    How do I make my computer respond to a situation on its own?
\end{frame}

\begin{frame}{Day \thesection\ Interview Question}
    \begin{itemize}[<+- | alert@+>]
        \item A guard has to take a wolf, a sheep, and a cabbage across a stream in a boat.  The boat has room for the guard and one of the other things.  The wolf will eat the sheep if left alone with it, and the sheep will eat the cabbage if left alone with it.  How can the guard get all these items across the river?
        \item Which item in this question is the problem?
        \item The guard can't leave the sheep unattended
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Where We've Been}
    \begin{itemize}[<+- | alert@+>]
        \item Topics we have covered so far:
        \begin{itemize}[<+- | alert@+>]
            \item Sequential instructions
            \item Limited vocabulary
            \item Types of errors -- syntax vs. semantic
            \item How the computer stores information
        \end{itemize}
        \item Types of computer statements we have mentioned so far:
        \begin{itemize}[<+- | alert@+>]
            \item Sequential statements
            \item Selection statements
            \item Iterative statements
        \end{itemize}
        \item Today we begin to focus on selection statements
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditions}
    \begin{itemize}[<+- | alert@+>]
        \item How did you decide whether to wear a jacket today?
        \item How will you decide whether to bring an umbrella tomorrow?
        \item What has to be true for you to come to this class?
        \item All of these things are \textit{conditions}
        \item In programming, a \textit{condition} is something that is either true or false
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditional Statements}
    \begin{itemize}[<+- | alert@+>]
        \item A \alert{\textit{conditional statement}} is an instruction that allows a computer to analyze a condition and make a decision
        \item Conditional statements are also called \textit{selection statements} and \textit{if statements}
        \item This is the format of a \alert{simple conditional statement}:\\
        \texttt{if} \emph{<condition>}\texttt{:}\\
        \texttt{\phantom{xxxx}}\textit{<statement>}
        \item The instructions in the \textit{statement} will run if the \emph{condition} is true
        \item If the \emph{condition} is false, nothing will happen
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditional Statements}
    \begin{itemize}[<+- | alert@+>]
        \item A condition usually compares two values, using a relational operator
        \item[] \begin{tabular}{cc}
        Relational Operator & Meaning\\
        \hline
        \texttt{>} & Greater than\\
        \texttt{>=} & Greater than or equal\\
        \texttt{<} & Less than\\
        \texttt{<=} & Less than or equal\\
        \texttt{==} & Equal\\
        \texttt{!=} & Not equal\\
        \hline
        \end{tabular}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditional Statements}
    \begin{itemize}[<+- | alert@+>]
        \item A condition generally compares the \alert{\textbf{present state}} of something with a \alert{\textbf{possible value}} of that thing
        \item When deciding to wear a jacket, for instance, you compare the \textit{present state} of the temperature outside with one \textit{possible value} of the temperature
        \item Based upon the possible value you choose, the condition will have an \alert{\textbf{outcome}} -- either something will happen, or nothing will happen
        \item Pay careful attention to the \textit{possible values} used when evaluating these conditions, and what the possible \textit{outcomes} are
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditional Statements}
    \begin{itemize}[<+- | alert@+>]
        \item Consider this conditional statement:\\
        \texttt{if temperature < 70:}\\
        \texttt{\phantom{xxxx}bring a jacket}
        \item What happens if the temperature is 65?
        \begin{itemize}[<+- | alert@+>]
            \item Bring a jacket
        \end{itemize}
        \item What happens if the temperature is 73?
        \begin{itemize}[<+- | alert@+>]
            \item Nothing
        \end{itemize}
        \item What happens if the temperature is 70?
        \begin{itemize}[<+- | alert@+>]
            \item Nothing
            \item That's called an \textit{edge case} or \textit{boundary condition}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditional Statements}
    \begin{itemize}[<+- | alert@+>]
        \item Complete the exercises on the handout \textbf{Day 6 Simple Conditionals Exercises}
        \item For each exercise, determine what \alert{test cases} to use first
        \begin{itemize}[<+- | alert@+>]
            \item Pick at least one \textit{possible value} that makes the condition \textit{true}
            \item Pick at least one \textit{possible value} that makes the condition \textit{false}
            \item Pick at least one \textit{edge case} -- a possible value that lies on the boundary between two \textit{outcomes}
        \end{itemize}
        \item Next, write the conditional
        \item Finally, test your conditional with the test cases you chose before
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditional Statements Example Exercise}
    \begin{itemize}[<+- | alert@+>]
        \item Example: ``study harder'' if the exam grade is below 80
        \item First, choose test cases and determine what results should correspond
        \begin{itemize}[<+- | alert@+>]
            \item Choose a number that's greater than 80 and a number that's less than 80
            \item Then choose an \textit{edge case}, which is 80 in this case
            \item Determine whether these values should result in ``study harder or not''
            \item An exam grade of 82 should not result in ``study harder'', but an exam grade of 77 should
            \item An exam grade of 80 is not below 80, and so there should be no output
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditional Statements Example Exercise}
    \begin{itemize}[<+- | alert@+>]
        \item Example: ``study harder'' if the exam grade is below 80
        \item Next, write the condition
        \begin{itemize}[<+- | alert@+>]
            \item Determine what values are being compared
            \item In this case, we're comparing the grade on an exam with the number 80
            \item Then determine what relationship between these two values you wish to examine
            \item You could use ``less than'' or ``greater than or equal'' or any of the others, as long as they are appropriate to the situation
            \item ``Below 80'' means ``less than 80'', so a condition like \texttt{exam\_grade~<~80} seems appropriate
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditional Statements Example Exercise}
    \begin{itemize}[<+- | alert@+>]
        \item Example: ``study harder'' if the exam grade is below 80
        \item Finally, test the condition
        \begin{itemize}[<+- | alert@+>]
            \item Replace the variable (in this case, \texttt{exam\_grade}) with each of your test cases, and determine the value of the condition
            \item Replacing \texttt{exam\_grade} with 82 results in the condition \texttt{82~<~80}, which is \textit{false}, and so there is no output
            \item When we developed the test case, we said  \textbf{An exam grade of 82 should not result in ``study harder''}, and this test confirmed that result
            \item \textit{So far}, the condition is working, but we have more tests to perform
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Conditional Statements Example Exercise}
    \begin{itemize}[<+- | alert@+>]
        \item Example: ``study harder'' if the exam grade is below 80
        \item Test the other two test cases:
        \begin{itemize}[<+- | alert@+>]
            \item Replacing \texttt{exam\_grade} with 77 results in the condition \texttt{77~<~80}, which is \textit{true}, and so the result is ``study harder'', which matches the test case we developed before
            \item Replacing \texttt{exam\_grade} with 80 results in the condition \texttt{80~<~80}, which is \textit{false}, and so there is no output, which matches the test case we developed before
        \end{itemize}
        \item If all test cases match your expectations, then the condition is correct!
        \item If not, go back to the beginning and see where the flaw in your logic is
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Simple Conditionals Exercises}
    \begin{itemize}
        \item Complete the exercises on the \textbf{Simple Conditionals Exercises} worksheet
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ If-Else Conditional Statements}
    \begin{itemize}[<+- | alert@+>]
        \item Sometimes, we want the computer to perform a task when the condition is false
        \item This is the format of an \alert{if-else conditional statement}:\\
        \texttt{if} \emph{<condition>}\texttt{:}\\
        \texttt{\phantom{xxxx}}\textit{<statement$_T$>}\\
        \texttt{else:}\\
        \texttt{\phantom{xxxx}}\textit{<statement$_F$>}\\
        \item The instructions in \textit{statement$_T$} will run if the \emph{condition} is true, and the instructions in \textit{statement$_F$} will be ignored
        \item If the \emph{condition} is false, then the instructions in \textit{statement$_F$} will run \textit{instead}, and the instructions in \textit{statement$_T$} will be ignored
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ If-Else Conditional Statements}
    \begin{itemize}[<+- | alert@+>]
        \item Consider this example:\\
        \texttt{if age >= 21:}\\
        \texttt{\phantom{xxxx}Sell customer beer}\\
        \texttt{else:}\\
        \texttt{\phantom{xxxx}Send customer home}\\
        \item Let's test the values 19, 20, 21, and 22 for \texttt{age}
        \begin{itemize}[<+- | alert@+>]
            \item \texttt{19 >= 21} is \textit{false}
            \item Send customer home
            \item \texttt{20 >= 21} is \textit{false}
            \item Send customer home
            \item \texttt{21 >= 21} is \textit{true}
            \item Sell customer beer
            \item \texttt{22 >= 21} is \textit{true}
            \item Sell customer beer
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ If-Else Conditional Statements}
    \begin{itemize}[<+- | alert@+>]
        \item Consider this example:\\
        \texttt{if hours\_worked > 40:}\\
        \texttt{\phantom{xxxx}Overtime pay}\\
        \texttt{else:}\\
        \texttt{\phantom{xxxx}Regular pay}\\
        \item What is the outcome for 30 hours worked?
        \item \texttt{30 > 40} is \textit{false}: Regular pay
        \item What is the outcome for 35 hours worked?
        \item \texttt{35 > 40} is \textit{false}: Regular pay
        \item What is the outcome for 40 hours worked?
        \item \texttt{40 > 40} is \textit{false}: Regular pay
        \item What is the outcome for 45 hours worked?
        \item \texttt{45 > 40} is \textit{true}: Overtime pay
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ If-Else Conditional Statements Exercises}
    \begin{itemize}
        \item Complete the exercises on the \textbf{If-Else Conditional Statements Exercises} worksheet
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Homework}
    \begin{itemize}
        \item Start work on \alert{Day \thesection--\ref{day:debuggingconditionals} Conditionals Homework}; it will be due after Day \ref{day:debuggingconditionals}
        \item Read the \alert{Lecture Notes} for Day \ref{day:debuggingconditionals}
    \end{itemize}
\end{frame}