% !TEX root = lecture.tex

\section{Day \thesection: Nested Conditionals and Complex Conditionals}

\begin{frame}{Day \thesection\ Introduction}
    \prob{I want my computer to decide what to do}
    \tools{Pen, paper, relational operators, conditional statements}
    \newtools{Decision trees, nested conditionals, Boolean operators}
    How do I make my computer respond to more interesting situations?
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item A condition can only hold one of two values: False or True
        \item A \textit{conditional statement}, therefore, can only lead us to one of two \textit{outcomes}
        \item How do we deal with a scenario that involves more than two outcomes?
        \item In a \alert{nested conditional}, some questions lead to asking more questions
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item Suppose a user has typed in a number into a Python program
        \item We want to know if the number is positive or negative
        \item Does this conditional statement solve the problem?\\
        \texttt{if the\_number > 0:}\\
        \texttt{\phantom{xxxx}It's positive}\\
        \texttt{else:}\\
        \texttt{\phantom{xxxx}It's negative}\\
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item One way to attack this problem is from the bottom up
        \item In the Lecture Notes, you saw a \alert{decision tree} like this:\\
        \begin{tikzpicture}[level distance=2cm]
\Tree[.{Are you a CS major}?
    \edge node[auto=right] {Yes};
    {You're a CS major}
    \edge node[auto=left] {No};
    [.{Are you an IT major}?
        \edge node[auto=right] {Yes};
        {You're an IT major}
        \edge node[auto=left] {No};
        {You're something else} ] ]
        \end{tikzpicture}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item We can use this tool by \alert{starting with the outcomes} and working up to the right questions
        \item We know there are three outcomes:\\
        \begin{tikzpicture}[level distance=2cm]
\Tree[.{\alert{Ask something smart}}
    \edge node[auto=right] {Yes};
    {It's a positive number}
    \edge node[auto=left] {No};
    [.{\alert{Ask something else smart}}
        \edge node[auto=right] {Yes};
        {It's a negative number}
        \edge node[auto=left] {No};
        {It's neither} ] ]
        \end{tikzpicture}
        \item Now we just have to ask something smart
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item Pick an outcome, and develop a question for it
        \item Develop your question so that one of the following things is true:
        \begin{itemize}[<+- | alert@+>]
            \item The answer ``Yes'' \textit{always} leads to this outcome, and the answer ``No'' \textit{never} does
            \item The answer ``Yes'' \textit{never} leads to this outcome, and the answer ``No'' \textit{always} does
        \end{itemize}
        \item Let's choose the outcome \alert{``It's a positive number''}
        \item What question will always help us decide whether this outcome is useful based on the possible value?
        \item Perhaps a question like ``Is the number greater than zero?''
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item Let's see:\\
        \begin{tikzpicture}[level distance=2cm]
\Tree[.{\alert{Is the number greater than zero?}}
    \edge node[auto=right] {Yes};
    {It's a positive number}
    \edge node[auto=left] {No};
    [.{Ask something else smart}
        \edge node[auto=right] {Yes};
        {It's a negative number}
        \edge node[auto=left] {No};
        {It's neither} ] ]
        \end{tikzpicture}
        \item That seems right; a number that isn't greater than zero can't be positive
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item Our progress so far:\\
        \begin{tikzpicture}[level distance=2cm]
\Tree[.{Is the number greater than zero?}
    \edge node[auto=right] {Yes};
    {It's a positive number}
    \edge node[auto=left] {No};
    [.{\alert{Ask something else smart}}
        \edge node[auto=right] {Yes};
        {It's a negative number}
        \edge node[auto=left] {No};
        {It's neither} ] ]
        \end{tikzpicture}
        \item What do we know once we get to the second question?
        \item We know that the number \textbf{is not greater than zero} -- if it were, the answer to the first question would have been yes
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item We now have just two outcomes to deal with
        \item By making a smart choice for the first question, we have reduced this problem to a \textit{simple conditional}, like you dealt with last class
        \item What question can we ask about a number that \textit{we already know isn't greater than zero} to determine whether it's negative, or neither negative nor positive?
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item Our decision tree is done:\\
        \begin{tikzpicture}[level distance=2cm]
\Tree[.{Is the number greater than zero?}
    \edge node[auto=right] {Yes};
    {It's a positive number}
    \edge node[auto=left] {No};
    [.{Is the number less than zero?}
        \edge node[auto=right] {Yes};
        {It's a negative number}
        \edge node[auto=left] {No};
        {It's neither} ] ]
        \end{tikzpicture}
        \item Now you can write your conditional statement just by reading your decision tree
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Nested Conditional Statements Exercises}
    \begin{itemize}
        \item Complete the exercises on the \textbf{Nested Conditional Statements Exercises} worksheet
        \item Remember to work efficiently!
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Complex Conditionals}
    \begin{itemize}[<+- | alert@+>]
        \item Sometimes, two things have to be true in order for an outcome to take place
        \item Sometimes, one of two things \textit{can} be true for an outcome to take place
        \item If you are trying to write a conditional statement with only one or two outcomes, you can only write one condtional statemnt -- no matter how many conditions have to be true
        \item This is where \textbf{complex conditional statements} come in
        \item \texttt{if foo > 5 and bar < 7:}\\
        \texttt{\phantom{xxxx}"yes"}
        \item \texttt{if baz <= 13 or quux > 19:}\\
        \texttt{\phantom{xxxx}"no"}
    \end{itemize}
\end{frame}

\begin{frame}{Day \thesection\ Complex Conditional Statements Exercises}
    \begin{itemize}
        \item Complete the exercises on the \textbf{Complex Conditional Statements Exercises} worksheet
        \item Remember to work efficiently!
    \end{itemize}
\end{frame}

\begin{frame}[standout]{Day \thesection\ Homework}
    \begin{itemize}
        \item Finish \alert{Day \ref{day:cond2}--\ref{day:debuggingconditionals} Conditionals Homework}; it will be due before the next class starts
    \end{itemize}
\end{frame}